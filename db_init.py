import sqlite3

connection = sqlite3.connect('messages.db')
cursor = connection.cursor()
cursor.execute('''CREATE TABLE IF NOT EXISTS messages
                    (id INTEGER PRIMARY KEY AUTOINCREMENT,
                    uid TEXT UNIQUE NOT NULL,
                    recipient_email TEXT NOT NULL,
                    message TEXT NOT NULL)''')
connection.commit()
connection.close()