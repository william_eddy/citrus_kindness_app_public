from flask import Flask, render_template, request, redirect, url_for
from flask_mail import Mail, Message
from dotenv import load_dotenv
load_dotenv()
import os
import sqlite3
import random
import string

app = Flask(__name__)

# Configure email settings
app.config['MAIL_SERVER'] = [EMAIL SERVER]
app.config['MAIL_PORT'] = [EMAIL SERVER PORT]
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USERNAME'] = [EMAIL ADDRESS]
app.config['MAIL_PASSWORD'] = [PASSWORD]
app.config['MAIL_DEFAULT_SENDER'] = [EMAIL ADDRESS AGAIN]

mail = Mail(app)

def send_email(recipient_email, message_id):

    html_content = render_template('email.html', messageLink = [WEBSITE ADDRESS] + "/view_message/" + message_id)

    msg = Message('Kindness Message', recipients=[recipient_email], html=html_content)
    mail.send(msg)

def generate_random_string(length):
    letters = string.ascii_letters
    return ''.join(random.choices(letters, k=length))

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        recipient_email = request.form['recipient_email']
        message = request.form['message']

        uid = generate_random_string(15);

        connection = sqlite3.connect('/home/citrusgroup/citrus_kindness_app/messages.db')
        cursor = connection.cursor()
        cursor.execute('INSERT INTO messages (UID, recipient_email, message) VALUES (?, ?, ?)',
                       (uid, recipient_email, message))
        connection.commit()
        connection.close()

        send_email(recipient_email, uid)

        return redirect(url_for('thanks'))

    return render_template('form.html')


@app.route('/thanks')
def thanks():
    return render_template('thanks.html')

@app.route('/privacy')
def privacy():
    return render_template('privacy.html')


@app.route('/view_message/<string:uid>')
def view_message(uid):
    connection = sqlite3.connect('/home/citrusgroup/citrus_kindness_app/messages.db')
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM messages WHERE uid = ?', (uid,))
    message = cursor.fetchone()
    connection.close()

    if message:
        return render_template('view_message.html', message=message)
    else:
        return "Message not found."
